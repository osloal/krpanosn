let krpano = null;

embedpano({
    swf:"krpano.swf",
     xml:"test.xml", 
     target:"pano", 
     html5:"auto", 
     mobilescale:1.0, 
     passQueryParameters:"xml,skin_settings.littleplanetintro,editmode",
     onready:krpanoReady_all,
});

function krpanoReady_all(krpano_interface){
    krpano = krpano_interface	
}

let app = new Vue({
    el:"#app",
    data:()=>{
        return{
            viewBox:[
                {
                    index:0,
                    name:'三维',
                    img:'../img/view_3d.png'
                },
                {
                    index:1,
                    name:'鸟瞰',
                    img:'../img/view_bird.png'
                },
                {
                    index:2,
                    name:'漫游',
                    img:'../img/view_browse.png'
                },
            ],
            viewItemIndex:2,
            imageOx:0,
            imageOy:0,
            imageOz:0,
            view:null,
        }
    },
    created() {
        
    },
    mounted() {
        
    },
    methods:{
        changeView(index){
            this.viewItemIndex = index;
            switch(index){
                case 0:
                    this.getInfo()
                    krpano.call('dollhouse_view');
                    break;
                case 1:
                    this.getInfo()
                    krpano.call('bird_view');
                    break
                case 2:
                    this.getInfo()
                    krpano.call("lookto(230,0,90,default,true,true);")
                    krpano.call("set(control.invert,false);")
krpano.call(`tween(view.tx|view.ty|view.tz|view.ox|view.oy|view.oz|view.fisheye,calc(800+'|'+image.oy+'|550|0|60|0|0'),1,default);`);
                    break;
            }
        },
        getInfo(){
            this.imageOx  = krpano.get("image.ox");
			this.imageOy  = krpano.get("image.oy");
			this.imageOz  = krpano.get("image.oz");

            this.view = krpano.get("view");
            console.log(this.view.tx);
            console.log(this.view.ty);
            console.log(this.view.tz);
            
        }
    }
})